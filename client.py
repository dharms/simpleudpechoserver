#simpleUDPEcho client
#This program implements a simple client for the echo server
# Douglas Harms

import socket
import sys

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# process command line arguments
argc = len(sys.argv)
if argc != 4:
    print ("usage: python3 client.py serverAddress serverPort message")
    sys.exit()
serverName = sys.argv[1]
port = int(sys.argv[2])
message = sys.argv[3]

#define server address
server_address = (serverName,port)

# Send data
sent = sock.sendto(message.encode(), server_address)

# Receive response
data, server = sock.recvfrom(4096)
print ('received "' +  data.decode() + '"' + ' from server')

sock.close()
