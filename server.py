# simpleUDPEcho server
# This program implements a simple echo server
# Douglas Harms

import socket
import sys

# Create a UDP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# process command line arguments
argc = len(sys.argv)
if argc != 2:
    print ("usage: python3 server.py serverPort")
    sys.exit()
port = int(sys.argv[1])

# Bind the socket to the port
server_address = ("", port)
sock.bind(server_address)

print ('Server starting on UDP port ' + str(server_address[1]))
print ("ctrl-c aborts server")

#process requests forever
try:
    while True:
        # wait for request from client
        data, address = sock.recvfrom(4096)

        # print some information about the request
        print ('received ' + str(len(data)) + ' bytes from ' + str(address) )
        print (data.decode())

        # echo the data back to the client
        sent = sock.sendto(data, address)
except:
    print("ctrl-c entered")

print ("Thanks for using this server!")
